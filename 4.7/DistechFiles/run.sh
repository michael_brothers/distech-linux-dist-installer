#!/bin/bash
COMMAND=$1

DISTECHCONTROLSFILES="/opt/Niagara/EC-NET4-4.7.110.32/Distech Controls Files/"
NIAGARAHOMESHARED=/opt/Niagara/EC-NET4-4.7.110.32/shared/ 
NIAGARAMODULES=/opt/Niagara/EC-NET4-4.7.110.32/modules/ 
NIAGARADEFAULTS=/opt/Niagara/EC-NET4-4.7.110.32/defaults/
DAEMONHOMESHARED=/home/niagara/Niagara4.7/distech/shared/ 
BASENIAGARADIR=/opt/Niagara/EC-NET4-4.7.110.32/

RECOVERYDIR=/home/niagara/recovery



make_recovery(){
echo "Making Recovery..."
sudo mkdir -p $RECOVERYDIR/modules && sudo cp -ra $NIAGARAMODULES. $RECOVERYDIR/modules
sudo mkdir -p $RECOVERYDIR/defaults && sudo cp -ra $NIAGARADEFAULTS.  $RECOVERYDIR/defaults
sudo mkdir -p "$RECOVERYDIR/Distech Controls Files" && sudo cp -ra "$DISTECHCONTROLSFILES". "$RECOVERYDIR/Distech Controls Files"
sudo mkdir -p $RECOVERYDIR/userhome/shared && sudo cp -ra $NIAGARAHOMESHARED. $RECOVERYDIR/userhome/shared
sudo mkdir -p $RECOVERYDIR/daemonhome/shared && sudo cp -ra $DAEMONHOMESHARED. $RECOVERYDIR/daemonhome/shared
}

copy_files(){
echo "Copying Files..."
sudo cp -ra ./modules/. $NIAGARAMODULES
sudo cp -ra ./defaults/. $NIAGARADEFAULTS
if [ -d "$DISTECHCONTROLSFILES" ]; then
	sudo cp -ra ./distribution/. "$DISTECHCONTROLSFILES"
else
	sudo mkdir "$DISTECHCONTROLSFILES" && sudo cp -ra ./distribution/. "$DISTECHCONTROLSFILES"
fi

if [ -d "$NIAGARAHOMESHARED" ]; then
	sudo cp -ra ./shared/. $NIAGARAHOMESHARED 
else 
	sudo mkdir $NIAGARAHOMESHARED && sudo cp -ra ./shared/. $NIAGARAHOMESHARED 
fi

sudo cp -ra ./shared/. $DAEMONHOMESHARED 
}

own_files(){
echo "Owning Files..."
sudo chown -R niagara:niagara $NIAGARAMODULES
sudo chown -R niagara:niagara $NIAGARADEFAULTS
sudo chown -R niagara:niagara "$DISTECHCONTROLSFILES"
sudo chown -R niagara:niagara $NIAGARAHOMESHARED 
sudo chown -R niagara:niagara $DAEMONHOMESHARED 
}

recover(){
echo "Recovering...."
sudo rm -rf $NIAGARAMODULES &&  sudo cp -ra $RECOVERYDIR/modules $BASENIAGARADIR
sudo rm -rf $NIAGARADEFAULTS && sudo cp -ra $RECOVERYDIR/defaults $BASENIAGARADIR
sudo rm -rf "$DISTECHCONTROLSFILES" && sudo cp -ra "$RECOVERYDIR/Distech Controls Files" $BASENIAGARADIR
sudo rm -rf $NIAGARAHOMESHARED && sudo cp -ra $RECOVERYDIR/userhome/shared $BASENIAGARADIR
sudo rm -rf $DAEMONHOMESHARED && sudo cp -ra $RECOVERYDIR/daemonhome/shared $BASENIAGARADIR
}

delete_recovery(){
echo "Deleting Recovery..."
sudo rm -rf /home/niagara/recovery
}

if [ "$COMMAND" = "install" ]; then
	make_recovery
	copy_files
	own_files
	echo "Installation complete. Please run sudo systemctl restart n4d to restart niagara daemon and apply changes"
elif [ "$COMMAND" = "recover" ]; then
	recover
	own_files
	echo "Recovery Complete. Run sudo systemctl restart n4d to restart niagara daemon and apply changes"
elif [ "$COMMAND" = "delete_recovery" ]; then
	delete_recovery
	echo "$RECOVERYDIR deleted"
else
	echo "Command not accepted"
fi
