# distech-linux-dist-installer

USE AT OWN RISK
** script to install files/modules needed for EC net supportpack

./run.sh install 
  - installs files from the corresponding directories where they need to go

./run.sh recover
  - deletes directories and reinstalls the original files

./run.sh delete_recovery
  - deletes the recovery folder. Originally saved under /home/niagara/recovery
